package com.percallgroup;

import com.percallgroup.docxedition.POIDocxEditor;
import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import com.percallgroup.enums.PdfLibraryEnum;
import com.percallgroup.factories.PdfConverterFactory;
import com.percallgroup.utils.AppUtils;
import com.percallgroup.utils.FileUtils;

import java.io.*;
import java.util.List;
import java.util.Map;

public class App {


    public static void main(String[] args) throws Exception {
        PdfLibraryEnum chosenLibrary = AppUtils.getChosenLibrary();

        Map<String, String> keysMap = AppUtils.buildKeysMap();
        List<String> filesPathsList = AppUtils.buildFilesPathsList();

        File modelFile = FileUtils.getModelFile();
        File sourceFile = FileUtils.getSourceFile();
        File targetFile = FileUtils.getTargetFile(chosenLibrary);

        FileUtils.cleanAndPrepareFiles(modelFile);

        POIDocxEditor docxEditor = new POIDocxEditor();
        docxEditor.editSourceFile(sourceFile, keysMap);

        PdfConverterFactory pdfConverterFactory = PdfConverterFactory.getInstance();
        DocxToPdfConverterInterface pdfConverter = pdfConverterFactory.getPdfConverter(chosenLibrary);
        pdfConverter.convertFile(sourceFile, targetFile, filesPathsList);
    }














}
