package com.percallgroup.factories;

import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import com.percallgroup.docxtopdfconverter.impl.Documents4JPdfConverterImpl;
import com.percallgroup.docxtopdfconverter.impl.Docx4JPdfConverterImpl;
import com.percallgroup.docxtopdfconverter.impl.ItextPdfConverterImpl;
import com.percallgroup.docxtopdfconverter.impl.POIPdfConverterImpl;
import com.percallgroup.enums.PdfLibraryEnum;

public class PdfConverterFactory {

    private static PdfConverterFactory instance = null;

    private PdfConverterFactory() {}

    public static PdfConverterFactory getInstance() {
        if (instance == null) {
            return new PdfConverterFactory();
        }
        return instance;
    }

    /**
     * Return a correctly instantiated pdf converter for the chosen library
     * @param chosenLibrary
     * @return
     */
    public DocxToPdfConverterInterface getPdfConverter(PdfLibraryEnum chosenLibrary) {
        switch (chosenLibrary) {

            case DOCX4J:
                return new Docx4JPdfConverterImpl();

            case DOCS4J:
                return new Documents4JPdfConverterImpl();

            case ITX:
                return new ItextPdfConverterImpl();

            case POI:
                return new POIPdfConverterImpl();

            default:
                throw new IllegalArgumentException("Unknown library");
        }
    }

}
