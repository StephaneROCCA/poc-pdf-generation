package com.percallgroup.docxedition;

import com.percallgroup.utils.AppUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class POIDocxEditor {

    public static final String KEY_CODE_START = "${";
    public static final String KEY_CODE_END = "}";
    public static final String KEY_REGEX = "\\$\\{.+?\\}";
    public static final String KEYS_PROPERTIES_PATH = "C:\\Users\\srocca\\IdeaProjects\\docxToPdf\\src\\main\\resources\\keys.properties";
    private Logger log = LoggerFactory.getLogger(POIDocxEditor.class);

    /**
     * Edit source file, replacing map keys by value in source file
     *
     * @param sourceFile
     * @param keysMap
     */
    public void editSourceFile(File sourceFile, Map<String, String> keysMap) {
        try (InputStream doc = new FileInputStream(sourceFile)) {
            XWPFDocument document = new XWPFDocument(doc);

            log.info("Editing document headers");
            editHeader(document, keysMap);

            log.info("Editing document tables");
            editTables(document.getTables(), keysMap);

            log.info("Editing document paragraphs");
            editParagraphs(document.getParagraphs(), keysMap);

            log.info("Editing document footer");
            editFooter(document, keysMap);

            writeFile(sourceFile, document);

        } catch (FileNotFoundException e) {
            log.error(String.format("%s not found.", sourceFile.getAbsolutePath()));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Edit document footer
     *
     * @param document
     * @param keysMap
     */
    private void editFooter(XWPFDocument document, Map<String, String> keysMap) {
        document.getFooterList().stream()
                .map(XWPFFooter::getTables)
                .forEach(xwpfTables -> editTables(xwpfTables, keysMap));
    }

    /**
     * Edit document header
     *
     * @param document
     * @param keysMap
     */
    private void editHeader(XWPFDocument document, Map<String, String> keysMap) {
        document.getHeaderList().stream()
                .map(XWPFHeader::getTables)
                .forEach(xwpfTables -> editTables(xwpfTables, keysMap));
    }

    /**
     * Write abstract document to source file
     *
     * @param sourceFile
     * @param document
     */
    private void writeFile(File sourceFile, XWPFDocument document) {
        try (FileOutputStream outputStream = new FileOutputStream(sourceFile)) {
            document.write(outputStream);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Edit all paragraphs from list, replacing map keys by value in each paragraph
     *
     * @param paragraphs
     * @param keysMap
     */
    private void editParagraphs(List<XWPFParagraph> paragraphs, Map<String, String> keysMap) {
        paragraphs.forEach(xwpfParagraph -> {
            String paragraphText = xwpfParagraph.getText();
            if (paragraphText.contains(KEY_CODE_START)) {
                TreeMap<Integer, XWPFRun> runsByPositionsInParagraph = getRunsByPositionsInParagraph(xwpfParagraph);
                Pattern pattern = Pattern.compile(KEY_REGEX);
                Matcher matcher = pattern.matcher(paragraphText);
                while (matcher.find()) {
                    int runStartIndex = matcher.start(0);
                    int runEndIndex = matcher.end(0);
                    SortedMap<Integer, XWPFRun> runsContainingKeysByPosition
                            = runsByPositionsInParagraph.subMap(runStartIndex, true, runEndIndex - 1, true);
                    String builtKey = buildKeyFromRuns(runsContainingKeysByPosition);
                    List<XWPFRun> runsContainingKey = getRunsContainingKeyElements(runsContainingKeysByPosition);
                    replaceFirstRunText(keysMap, builtKey, runsContainingKey);
                    deleteNextRunsText(runsContainingKey);
                }
            }
        });

    }

    /**
     * Set key value in first run text
     *
     * @param keysMap
     * @param builtKey
     * @param runsContainingKey
     */
    private void replaceFirstRunText(Map<String, String> keysMap, String builtKey, List<XWPFRun> runsContainingKey) {
        Optional<XWPFRun> optional = runsContainingKey.stream()
                .distinct()
                .findFirst();
        optional.ifPresent(xwpfRun -> {
            xwpfRun.setText(keysMap.get(builtKey), 0);
        });
    }

    /**
     * Set empty text in the next runs previously containing key element
     *
     * @param runsContainingKey
     */
    private void deleteNextRunsText(List<XWPFRun> runsContainingKey) {
        IntStream.range(0, runsContainingKey.size())
                .filter(i -> i != 0)
                .mapToObj(runsContainingKey::get)
                .forEach(xwpfRun -> xwpfRun.setText("", 0));
    }


    /**
     * Check if key is in valid format
     *
     * @param complete
     * @return
     */
    private boolean isKeyInvalid(String complete) {
        return !complete.startsWith(KEY_CODE_START) && !complete.endsWith(KEY_CODE_END);
    }

    /**
     * Build key from paragraph runs
     *
     * @param runsByPositionsInParagraph
     * @return
     */
    private String buildKeyFromRuns(SortedMap<Integer, XWPFRun> runsByPositionsInParagraph) {
        //  Get the unique runs containing key elements
        List<XWPFRun> runsContainingKeyElements = getRunsContainingKeyElements(runsByPositionsInParagraph);
        //  Build key from runs text
        String builtKey = runsContainingKeyElements.stream()
                .map(run -> run.getText(0).trim()).collect(Collectors.joining());
        //  Validate key or throw an exception
        if (isKeyInvalid(builtKey)) {
            log.error(String.format("Invalid key = %s", builtKey));
            throw new IllegalStateException("Incorrect key format");
        }
        return builtKey;
    }

    /**
     * Build a map linkng a run to its position in paragraph texte
     *
     * @param xwpfParagraph
     * @return
     */
    private TreeMap<Integer, XWPFRun> getRunsByPositionsInParagraph(XWPFParagraph xwpfParagraph) {
        int position = 0;
        TreeMap<Integer, XWPFRun> runsByPositions = new TreeMap<>();
        for (XWPFRun currentRun : xwpfParagraph.getRuns()) {
            String currentRunText = currentRun.text();
            if (currentRunText != null && currentRunText.length() > 0) {
                for (int i = 0; i < currentRunText.length(); i++) {
                    runsByPositions.put(position + i, currentRun);
                }
                position += currentRunText.length();
            }
        }
        return runsByPositions;
    }

    /**
     * Extract unique runs containing key elements
     *
     * @param runsByPositionsInParagraph
     * @return
     */
    private List<XWPFRun> getRunsContainingKeyElements(SortedMap<Integer, XWPFRun> runsByPositionsInParagraph) {
        return runsByPositionsInParagraph.values().stream()
                .distinct()
                .collect(Collectors.toList());
    }


    /**
     * Edit all tables from list, replacing map keys by value in each table paragraph
     *
     * @param tables
     * @param keysMap
     */
    private void editTables(List<XWPFTable> tables, Map<String, String> keysMap) {
        tables.stream()
                .map(XWPFTable::getRows)
                .flatMap(Collection::stream)
                .map(XWPFTableRow::getTableCells)
                .flatMap(Collection::stream)
                .map(XWPFTableCell::getParagraphs)
                .forEach(xwpfParagraphs -> editParagraphs(xwpfParagraphs, keysMap));
    }

    /**
     * Extract keys from document and populate keys.properties
     * @param stringFromRun
     */
    private void extractKeysFromString(String stringFromRun) {
        String propertiesFilePath = KEYS_PROPERTIES_PATH;
        Properties properties = AppUtils.loadProperties(propertiesFilePath);
        Pattern pattern = Pattern.compile("(\\{.*?\\})");
        Matcher matcher = pattern.matcher(stringFromRun);
        while (matcher.find()) {
            String key = matcher.group();
            assert properties != null;
            properties.setProperty(key, key.toUpperCase());
            try (OutputStream outputStream = new FileOutputStream(propertiesFilePath)) {
                properties.store(outputStream, null);
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }
}
