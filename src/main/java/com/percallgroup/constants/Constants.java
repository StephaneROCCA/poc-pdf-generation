package com.percallgroup.constants;

import com.documents4j.api.DocumentType;

public class Constants {

    private Constants() {}

    public static final String          APP_PROPERTIES = "app.properties";
    public static final String          KEYSMAP_PROPERTIES = "keys.properties";
    public static final String          BASE_FOLDER_PATH = "/home/workDir/";
    public static final String          SOURCE_FILE_PATH = BASE_FOLDER_PATH + "test.docx";
    public static final String          MODEL_FILE_PATH = BASE_FOLDER_PATH + "TNpourPiOTestFinish.docx";
    public static final String          EMBEDDED_FILE_PATH = BASE_FOLDER_PATH + "embedded";
    public static final String          EMBEDDED_FILE_EXTENSION = ".xls";
    public static final String          EMBEDDED_FILE_1_PATH = EMBEDDED_FILE_PATH + 1 + EMBEDDED_FILE_EXTENSION;
    public static final String          EMBEDDED_FILE_2_PATH = EMBEDDED_FILE_PATH + 2 + EMBEDDED_FILE_EXTENSION;
    public static final String          EMBEDDED_FILE_3_PATH = EMBEDDED_FILE_PATH + 3 + EMBEDDED_FILE_EXTENSION;
    public static final DocumentType    SOURCE_FILE_DOCUMENT_TYPE = DocumentType.MS_WORD;
    public static final DocumentType    TARGET_FILE_DOCUMENT_TYPE = DocumentType.PDF;
    public static final String          CONVERSION_SERVER_URL = "http://localhost:9998";

}
