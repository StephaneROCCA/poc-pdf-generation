package com.percallgroup.utils;

import com.percallgroup.enums.PdfLibraryEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.*;

import static com.percallgroup.constants.Constants.*;

public class AppUtils {

    private static Logger log = LoggerFactory.getLogger(AppUtils.class);

    private AppUtils() {
    }

    /**
     * Build map to replace keys with values in document
     *
     * @return
     */
    public static Map<String, String> buildKeysMap() {
        Map<String, String> keysMap = new HashMap<>();
        Properties properties = loadProperties(KEYSMAP_PROPERTIES);
        if (properties != null) {
            Set<Object> keySet = properties.keySet();
            keySet.forEach(key -> {
                String value = properties.getProperty(key.toString());
                keysMap.put(key.toString(), value);
            });
        } else {
            keysMap.put("{key}", "Valeur de KEY1");
            keysMap.put("{key2}", "Valeur de KEY2");
        }

        return keysMap;
    }

    public static Properties loadProperties(String propertiesFilePath) {
        String propertiesLocalPath = "./" + propertiesFilePath;
        if (Paths.get(propertiesLocalPath).toFile().exists()) {
            try (InputStream input = new FileInputStream(propertiesLocalPath)) {
                Properties properties = new Properties();
                properties.load(input);

                return properties;
            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        } else {
            try (InputStream input = AppUtils.class.getClassLoader().getResourceAsStream(propertiesFilePath)) {
                Properties properties = new Properties();
                properties.load(input);

                return properties;
            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        }

        return null;
    }

    public static PdfLibraryEnum getChosenLibrary() {
        Properties properties = loadProperties(APP_PROPERTIES);
        assert properties != null;
        String property = properties.getProperty("app.chosen_library");

        return PdfLibraryEnum.valueOf(property);
    }

    public static Boolean getDocx4JisLocal() {
        Properties properties = loadProperties(APP_PROPERTIES);
        assert properties != null;
        String property = properties.getProperty("app.docx4j.is_local");

        return Boolean.parseBoolean(property);
    }

    public static List<String> buildFilesPathsList() {
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            list.add(EMBEDDED_FILE_PATH + i + EMBEDDED_FILE_EXTENSION);
        }
        return list;
    }
}
