package com.percallgroup.utils;

import com.percallgroup.constants.Constants;
import com.percallgroup.enums.PdfLibraryEnum;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.percallgroup.constants.Constants.EMBEDDED_FILE_EXTENSION;
import static com.percallgroup.constants.Constants.EMBEDDED_FILE_PATH;

public class FileUtils {

    private static Logger log = LoggerFactory.getLogger(FileUtils.class);

    /**
     * Remove all previous generated files during test phases
     *
     * @param modelFile
     * @throws IOException
     */
    public static void cleanAndPrepareFiles(File modelFile) {
        cleanFiles(modelFile);
        prepareFiles(modelFile);
    }


    public static File getModelFile() {
        return new File(Constants.MODEL_FILE_PATH);
    }

    public static File getSourceFile() {
        return new File(Constants.SOURCE_FILE_PATH);
    }

    public static File getTargetFile(PdfLibraryEnum chosenLibrary) {
        return new File(Constants.SOURCE_FILE_PATH + chosenLibrary.name() + ".pdf");
    }

    /**
     * Create a working copy from file model
     *
     * @param modelFile
     */
    private static void prepareFiles(File modelFile) {
        try {
            log.info("Copying \"" + Constants.MODEL_FILE_PATH + "\" to \"" + Constants.SOURCE_FILE_PATH);
            Files.copy(modelFile.toPath(), new FileOutputStream(Constants.SOURCE_FILE_PATH));
            //  Creating 3 dummies xls file to test file embed procedure
            for (int i = 1; i <= 3; i++) {
                HSSFWorkbook workbook = new HSSFWorkbook();
                workbook.createSheet("Sheet 1")
                        .createRow(0)
                        .createCell(0)
                        .setCellValue("Contenu du fichier XLS n°" + i);

                try (OutputStream outputStream = new FileOutputStream(EMBEDDED_FILE_PATH + i + EMBEDDED_FILE_EXTENSION)) {
                    workbook.write(outputStream);
                }
            }
        } catch (IOException e) {
            log.error(String.format("Error copying files. %s", e.getMessage()));
        }
    }

    /**
     * Clean all generated files
     *
     * @param modelFile
     */
    private static void cleanFiles(File modelFile) {
        log.info("Deleting previous generated files");
        try {
            Files.list(Paths.get(Constants.BASE_FOLDER_PATH))
                    .filter(path -> !path.equals(modelFile.toPath()))
                    .forEach(path -> {
                        try {
                            Files.delete(path);
                        } catch (IOException e) {
                            log.error(String.format("Error deleting %s.", path));
                        }
                    });
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

}
