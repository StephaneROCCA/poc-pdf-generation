package com.percallgroup.docxtopdfconverter.impl;

import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.*;
import java.util.List;

public class POIPdfConverterImpl implements DocxToPdfConverterInterface {

    private Logger log = LoggerFactory.getLogger(POIPdfConverterImpl.class);

    @Override
    public void convertFile(File sourceFile, File targetFile, List<String> filesPathList) {
        log.info("Converting file to PDF with POI.");
        try {
            InputStream doc = new FileInputStream(sourceFile);
            XWPFDocument document = new XWPFDocument(doc);
            OutputStream out = new FileOutputStream(targetFile);
            PdfConverter.getInstance().convert(document, out, PdfOptions.create());
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
        try {
            log.info(String.format("Conversion with POI done. Opening %s", targetFile.getAbsolutePath()));
            Desktop.getDesktop().open(targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
