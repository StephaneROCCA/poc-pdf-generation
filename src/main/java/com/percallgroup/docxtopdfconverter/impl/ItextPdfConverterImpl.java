package com.percallgroup.docxtopdfconverter.impl;

import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class ItextPdfConverterImpl implements DocxToPdfConverterInterface {

    private Logger log = LoggerFactory.getLogger(ItextPdfConverterImpl.class);

    @Override
    public void convertFile(File sourceFile, File targetFile, List<String> filesPathList) {
        try (InputStream doc = new FileInputStream(sourceFile)) {
            XWPFDocument wordDocument = new XWPFDocument(doc);
            String text = new XWPFWordExtractor(wordDocument).getText();
            text = text.replace("\n", "").replace("\r", "").replace("\t", " ");
            PDDocument pdfDocument = new PDDocument();
            PDPage page = new PDPage();
            pdfDocument.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(pdfDocument, page);
            contentStream.beginText();
            contentStream.setFont(PDType1Font.COURIER, 12);
            contentStream.showText(text);
            contentStream.endText();
            contentStream.close();
            pdfDocument.save(targetFile);

        } catch (FileNotFoundException e) {
            log.error(String.format("%s file is not found"), sourceFile.getAbsolutePath());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
