package com.percallgroup.docxtopdfconverter.impl;

import com.documents4j.api.IConverter;
import com.documents4j.builder.ConverterServerBuilder;
import com.documents4j.job.LocalConverter;
import com.documents4j.job.RemoteConverter;
import com.percallgroup.constants.Constants;
import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Documents4JPdfConverterImpl implements DocxToPdfConverterInterface {

    private Logger log = LoggerFactory.getLogger(Documents4JPdfConverterImpl.class);

    @Override
    public void convertFile(File sourceFile, File targetFile, List<String> filesPathList) {
        try {
            HttpServer server = ConverterServerBuilder.make(Constants.CONVERSION_SERVER_URL);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Converting file with D4J");
        IConverter iConverter = getIConverter(false);
        iConverter.convert(sourceFile)
                .as(Constants.SOURCE_FILE_DOCUMENT_TYPE)
                .to(targetFile)
                .as(Constants.TARGET_FILE_DOCUMENT_TYPE)
                .schedule();
    }

    /**
     * Get Documents4J LocalConverter
     *
     * @return
     * @param isLocal
     */
    private  IConverter getIConverter(boolean isLocal) {
        if (isLocal) {
            return LocalConverter.builder()
                    .baseFolder(new File(Constants.BASE_FOLDER_PATH))
                    .workerPool(20, 25, 2, TimeUnit.SECONDS)
                    .processTimeout(5, TimeUnit.SECONDS)
                    .build();
        } else {
            return RemoteConverter.builder()
                    .baseFolder(new File(Constants.BASE_FOLDER_PATH))
                    .workerPool(20, 25, 2, TimeUnit.SECONDS)
                    .requestTimeout(10, TimeUnit.SECONDS)
                    .baseUri(Constants.CONVERSION_SERVER_URL)
                    .build();
        }
    }
}
