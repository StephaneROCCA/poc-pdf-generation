package com.percallgroup.docxtopdfconverter.impl;

import com.percallgroup.docxtopdfconverter.DocxToPdfConverterInterface;
import com.percallgroup.utils.AppUtils;
import org.apache.fop.apps.FOUserAgent;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import org.docx4j.Docx4J;
import org.docx4j.convert.out.FOSettings;
import org.docx4j.convert.out.fo.renderers.FORendererApacheFOP;
import org.docx4j.fonts.BestMatchingMapper;
import org.docx4j.fonts.Mapper;
import org.docx4j.fonts.PhysicalFont;
import org.docx4j.fonts.PhysicalFonts;
import org.docx4j.model.fields.FieldUpdater;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Docx4JPdfConverterImpl implements DocxToPdfConverterInterface {

    private Logger log = LoggerFactory.getLogger(Docx4JPdfConverterImpl.class);

    @Override
    public void convertFile(File sourceFile, File targetFile, List<String> filesPathsList) throws Exception {
        boolean isLocal = AppUtils.getDocx4JisLocal();
        if (isLocal) {
            autonomConversion(sourceFile, targetFile);
        } else {
            convertWithServer(sourceFile, targetFile);
        }
        addEmbeddedFiles(targetFile, filesPathsList);
    }


    /**
     * Convert file using dockerized conversion server
     *
     * @param sourceFile
     * @param targetFile
     * @throws Docx4JException
     * @throws FileNotFoundException
     */
    private void convertWithServer(File sourceFile, File targetFile) throws Docx4JException, FileNotFoundException {
        log.info("Converting with Docx4J conversion server.");
        WordprocessingMLPackage wordprocessingMLPackage = Docx4J.load(sourceFile);
        Docx4J.toPDF(wordprocessingMLPackage, new FileOutputStream(targetFile));
    }

    /**
     * Convert source file to target file locally without any exterior library
     *
     * @param sourceFile
     * @param targetFile
     * @throws Exception
     */
    private void autonomConversion(File sourceFile, File targetFile) throws Exception {
        log.info("Converting with Docx4J locally.");
        //  Don't save xml
        boolean saveFO = false;
        String regex = ".*(calibri|camb|cour|arial|times|comic|georgia|impact|LSANS|pala|tahoma|trebuc|verdana|symbol|webdings|wingding).*";
        PhysicalFonts.setRegex(regex);

        // Document loading (required)
        WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(sourceFile);
        // Refresh the values of DOCPROPERTY fields
        FieldUpdater updater = new FieldUpdater(wordMLPackage);
        updater.update(true);

        // Set up font mapper (optional)
        Mapper fontMapper = new BestMatchingMapper();
        wordMLPackage.setFontMapper(fontMapper);
        PhysicalFont font = PhysicalFonts.get("Calibri");
//        // make sure this is in your regex (if any)!!!
        if (font != null) {
            fontMapper.put("AvantGarde", font);
            fontMapper.put("Courrier", font);
            fontMapper.put("Arial", font);
            fontMapper.put("MS Sans Serif", font);
            fontMapper.put("sans-serif", font);
        }
        fontMapper.put("Libian SC Regular", PhysicalFonts.get("SimSun"));

        // FO exporter setup (required)
        FOSettings foSettings = Docx4J.createFOSettings();
        if (saveFO) {
            String absolutePath = sourceFile.getAbsolutePath();
            foSettings.setFoDumpFile(new File(absolutePath + ".fo"));
        }
        foSettings.setWmlPackage(wordMLPackage);

        FOUserAgent foUserAgent = FORendererApacheFOP.getFOUserAgent(foSettings);
        // configure foUserAgent as desired
        foUserAgent.setTitle("my title");

        // exporter writes to an OutputStream.
        OutputStream os = new FileOutputStream(targetFile);

        // Don't care what type of exporter you use
        Docx4J.toFO(foSettings, os, Docx4J.FLAG_EXPORT_PREFER_XSL);

        log.info(String.format("Saved : %s", targetFile.getName()));

        // Clean up, so any ObfuscatedFontPart temp files can be deleted
        if (wordMLPackage.getMainDocumentPart().getFontTablePart() != null) {
            wordMLPackage.getMainDocumentPart().getFontTablePart().deleteEmbeddedFontTempFiles();
        }
        // This would also do it, via finalize() methods
        updater = null;
        foSettings = null;
        wordMLPackage = null;
    }

    /**
     * Add
     *
     * @param targetFile
     * @param filesPathsList
     */
    public void addEmbeddedFiles(File targetFile, List<String> filesPathsList) {
        Map efMap = new HashMap<>();
        filesPathsList.forEach(filePath -> {
            PDEmbeddedFilesNameTreeNode efTree = new PDEmbeddedFilesNameTreeNode();
            PDComplexFileSpecification fs = new PDComplexFileSpecification();
            PDDocument doc = null;
            try {
                doc = PDDocument.load(targetFile);
            } catch (IOException e) {
                log.error(e.getMessage());
            }
            PDDocumentNameDictionary names = new PDDocumentNameDictionary(doc.getDocumentCatalog());
            fs.setFile(filePath);
            try (InputStream inputStream = new FileInputStream(filePath)) {
                PDEmbeddedFile embeddedFile = new PDEmbeddedFile(doc, inputStream);
                embeddedFile.setSubtype("text/plain");
                embeddedFile.setCreationDate(new GregorianCalendar());
                fs.setEmbeddedFile(embeddedFile);
                efMap.put(filePath, fs);
                efTree.setNames(efMap);
                names.setEmbeddedFiles(efTree);
                doc.getDocumentCatalog().setNames(names);
                doc.save(targetFile);
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        });
    }
}
