package com.percallgroup.docxtopdfconverter;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;

import java.io.File;
import java.util.List;

public interface DocxToPdfConverterInterface {

    /**
     * Convert source docx file to target file in pdf format
     * @param sourceFile
     * @param targetFile
     */
    public void convertFile(File sourceFile, File targetFile, List<String> filesPathsList) throws Exception;
}
