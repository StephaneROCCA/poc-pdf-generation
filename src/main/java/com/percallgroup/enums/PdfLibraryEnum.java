package com.percallgroup.enums;

public enum PdfLibraryEnum {
    DOCS4J("Documents4J"),
    DOCX4J("Docx4J"),
    ITX("iText"),
    PBOX("PdfBox"),
    POI("POI");

    private String libraryName;

    PdfLibraryEnum(String libraryName) {
        this.libraryName = libraryName;
    }

    public String getLibraryName() {
        return libraryName;
    }
}
